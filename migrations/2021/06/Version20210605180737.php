<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210605180737 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'camp-wide-game.overview', 'hash' => '039e7b0acffd9df3811e7bfd13fe2c72', 'module' => 'admin', 'language_id' => 1, 'singular' => 'CTH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'camp-wide-game.overview.title', 'hash' => '49431101eeb03cb4ff3dd8734999d41a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Celotáborové hry|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview.action.overview-part', 'hash' => 'c5bce8943edef031bb8fd75faa239d55', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled součástí CTH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview.action.new', 'hash' => 'a71ec3dbd871f3a904cb887915a1fd87', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit CTH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview.name', 'hash' => '35c34974dbffeef2447d8154367f917f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview.parts', 'hash' => 'f0b3bbda6d3827a34f8450fb6949696b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Součásti', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview.is-active', 'hash' => '2f3b62b3c338b03b384c4008f69bbd85', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'camp-wide-game.overview-part.title', 'hash' => 'c189d4fc256ebc1d5b88dc19593c01fc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Součásti CTH|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview-part.action.overview', 'hash' => 'eaff8e09fd614f29dc1ccd9899847628', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Přehled CTH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview-part.action.new', 'hash' => '5ee5076a57b88ead0b5906b65c4fbe93', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit součást CTH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview-part.name', 'hash' => '886747740c7b81d29eeac521372c8192', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview-part.is-active', 'hash' => '1e95599ca05103fb7bc83b4cb82089ce', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.title', 'hash' => '518f88e3c04990903ced224967ac558c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení součásti CTH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.name', 'hash' => 'fad06e09f30cea753df6edb294c93613', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.name.req', 'hash' => 'c66b97919fd4b30395332d68bde76eeb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.is-for-all-camp-wide-game', 'hash' => 'f28c065590571d2e7fa54b875f528537', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zobrazit u všech táborů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.camp-wide-games', 'hash' => 'c2662cfc4d502973da7b044ec7d73dda', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zobrazit u táborů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.camp-wide-games.placeholder', 'hash' => '6e88ab1b9ff60c9f2fe51093a446d9f7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte tábory', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.camp-wide-games.no-result', 'hash' => '27ec4fa9162a58163ed89e6749bfdd7e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nebyly nalezeny žádné tábory: ', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.camp-wide-games.hint', 'hash' => 'e22b3baf75f37908a7798139a68b233d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'U zvolených táborů se zobrazí tato součást v detailu. Vybrat můžete libovolný počet táborů. Tato možnost se hodí v případě, že se jedná o specifickou součást, která se nemá zobrazovat všude.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.camp-wide-games-exclude', 'hash' => 'aee79d6c78321eb8f1f57d5e4ce51281', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyloučit u táborů', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.camp-wide-games-exclude.placeholder', 'hash' => '6f6afc5f625138617ec0c2b12e41ebac', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vyberte tábory', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.camp-wide-games-exclude.no-result', 'hash' => '6b5de370609e4e32322a75593965d501', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nebyly nalezeny žádné tábory: ', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.camp-wide-games-exclude.hint', 'hash' => 'da8c73af53fad57ed0e40a72527f3b32', 'module' => 'admin', 'language_id' => 1, 'singular' => 'U zvolených táborů se nezobrazí tato součást v detailu. Vybrat můžete libovolný počet táborů. Tato možnost se hodí v případě, že se má zobrazit součást u všech táborů, ale některé mají být vyloučeny.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.content', 'hash' => '39b50038e2833f144e0973ee669caabb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.is-active', 'hash' => '52ad34a3a837387715186582bc9d24a4', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.send', 'hash' => 'b98c12a735e312c22b5c9adbaa430ef6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.send-back', 'hash' => 'e945d7a66325283f9f6d5af474f0ac49', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.back', 'hash' => 'c4d4498b5589fba900e329e2b4e576ea', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.title - %s', 'hash' => 'f86f955a69a1dea23a63b02fd0b5dd19', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace součásti CTH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.flash.success.create', 'hash' => 'fd7ab7456e80fd7355ef6da2f2368579', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Součást CTH byla úspěšně založena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game-part.edit.flash.success.update', 'hash' => 'b745e0341d36340abc732554a542a70c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Součást CTH byla úspěšně upravena.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview-part.action.edit', 'hash' => 'd886168a77b1c64de6e24360448e18ea', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview-part.action.flash.sort.success', 'hash' => 'b93c57a3ded71188d81b3992a1af63fc', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Pořadí součástí CTH bylo upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.title', 'hash' => '6a17236a53025bcb6f86aaaf94ceab00', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení CTH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.name', 'hash' => 'd85515df281d031378d4a98bffffbea6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.name.req', 'hash' => 'aa53c9dfefb43be7397400c5f93da031', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.image-preview', 'hash' => '41cdcb021850096f4805c06b1df00888', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Náhled CTH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.image-preview.rule-image', 'hash' => '1cb67c81c801e7b7ba411d3e02527ae1', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vybraný soubor není platný obrázek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.content', 'hash' => 'c58e3e06bff67eb0011c571be699659a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Popis', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.is-active', 'hash' => 'b01c406473259fa82527b1a9927b8225', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.send', 'hash' => '385182a9beb0919cc6f1565179c276cb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.send-back', 'hash' => 'ffa431de8867499dcfa4bc1cebd68c21', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.back', 'hash' => '7e84fd3bd3a9106ee6ed1b11d5a20918', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.title - %s', 'hash' => 'e5adeeb46e662ec9fa7af51f10eaeaab', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace CTH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.flash.success.create', 'hash' => '2d41fc71660be3180d339b172246d2b7', 'module' => 'admin', 'language_id' => 1, 'singular' => 'CTH bylo úspěšně založeno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.camp-wide-game.edit.flash.success.update', 'hash' => 'f97c96024b9d6d78cd589ee35c257ea6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'CTH bylo úspěšně upraveno.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview.action.edit', 'hash' => '78715a461b4444bfc64ca41deae25fc3', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview.action.remove', 'hash' => 'e9e0a7241dfb8c61d7b6b17e39213d4d', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview.action.remove.title', 'hash' => '78b9c3107f428d5aa052b5753596b9aa', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Odstranit CTH', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview.action.remove.confirmation - %s', 'hash' => 'ea35431f2ca7d7c8d58ba027f2c23459', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Opravdu chcete odstranit CTH "%s"?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.camp-wide-game.overview.action.flash.remove.success "%s"', 'hash' => 'f7855acacbf8cb077cf2ad6e909dcac5', 'module' => 'admin', 'language_id' => 1, 'singular' => 'CTH bylo úspěšně odebráno.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
