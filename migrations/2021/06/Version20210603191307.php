<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210603191307 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE camp_wide_game (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, image_preview VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE camp_wide_game_part (id INT AUTO_INCREMENT NOT NULL, is_for_all_camp_wide_game TINYINT(1) DEFAULT \'0\' NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, sequence INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE camp_wide_game_part_rel (camp_wide_game_part_id INT NOT NULL, camp_wide_game_id INT NOT NULL, INDEX IDX_A9162D5409779C8 (camp_wide_game_part_id), INDEX IDX_A9162D5444AC0F9 (camp_wide_game_id), PRIMARY KEY(camp_wide_game_part_id, camp_wide_game_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE camp_wide_game_part_exlude_rel (camp_wide_game_part_id INT NOT NULL, camp_wide_game_id INT NOT NULL, INDEX IDX_DEFFD718409779C8 (camp_wide_game_part_id), INDEX IDX_DEFFD718444AC0F9 (camp_wide_game_id), PRIMARY KEY(camp_wide_game_part_id, camp_wide_game_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE camp_wide_game_part_rel ADD CONSTRAINT FK_A9162D5409779C8 FOREIGN KEY (camp_wide_game_part_id) REFERENCES camp_wide_game_part (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE camp_wide_game_part_rel ADD CONSTRAINT FK_A9162D5444AC0F9 FOREIGN KEY (camp_wide_game_id) REFERENCES camp_wide_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE camp_wide_game_part_exlude_rel ADD CONSTRAINT FK_DEFFD718409779C8 FOREIGN KEY (camp_wide_game_part_id) REFERENCES camp_wide_game_part (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE camp_wide_game_part_exlude_rel ADD CONSTRAINT FK_DEFFD718444AC0F9 FOREIGN KEY (camp_wide_game_id) REFERENCES camp_wide_game (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE camp_wide_game_part_rel DROP FOREIGN KEY FK_A9162D5444AC0F9');
        $this->addSql('ALTER TABLE camp_wide_game_part_exlude_rel DROP FOREIGN KEY FK_DEFFD718444AC0F9');
        $this->addSql('ALTER TABLE camp_wide_game_part_rel DROP FOREIGN KEY FK_A9162D5409779C8');
        $this->addSql('ALTER TABLE camp_wide_game_part_exlude_rel DROP FOREIGN KEY FK_DEFFD718409779C8');
        $this->addSql('DROP TABLE camp_wide_game');
        $this->addSql('DROP TABLE camp_wide_game_part');
        $this->addSql('DROP TABLE camp_wide_game_part_rel');
        $this->addSql('DROP TABLE camp_wide_game_part_exlude_rel');
    }
}
