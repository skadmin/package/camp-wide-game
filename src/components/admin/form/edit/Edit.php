<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Doctrine\User\User;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nette\Utils\DateTime;
use Nette\Utils\Validators;
use Skadmin\CampWideGame\BaseControl;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGame;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGameFacade;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGamePart;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGamePartFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use function explode;
use function is_bool;
use function sprintf;

/**
 * Class Edit
 */
class Edit extends FormWithUserControl
{
    use APackageControl;

    /** @var LoaderFactory */
    private $webLoader;

    /** @var ImageStorage */
    private $imageStorage;

    /** @var CampWideGameFacade */
    private $facade;

    /** @var CampWideGamePartFacade */
    private $facadeCampWideGamePart;

    /** @var User */
    private $user;

    /** @var CampWideGame */
    private $campWideGame;

    public function __construct(?int $id, CampWideGameFacade $facade, CampWideGamePartFacade $facadeCampWideGamePart, Translator $translator, LoaderFactory $webLoader, ImageStorage $imageStorage, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade                 = $facade;
        $this->facadeCampWideGamePart = $facadeCampWideGamePart;
        $this->webLoader              = $webLoader;
        $this->imageStorage           = $imageStorage;
        $this->user                   = $this->loggedUser->getIdentity(); //@phpstan-ignore-line

        $this->campWideGame = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->campWideGame->isLoaded()) {
            return new SimpleTranslation('form.camp-wide-game.edit.title - %s', $this->campWideGame->getName());
        }

        return 'form.camp-wide-game.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss() : array
    {
        return [
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->campWideGame = $this->campWideGame;
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        // DATA
        $dataCampWideGame = $this->facade->getPairs('id', 'name');
        if ($this->campWideGame->isLoaded()) {
            unset($dataCampWideGame[$this->campWideGame->getId()]);
        }

        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.camp-wide-game.edit.name')
            ->setRequired('form.camp-wide-game.edit.name.req');
        $form->addTextArea('content', 'form.camp-wide-game.edit.content');

        $form->addCheckbox('is_active', 'form.camp-wide-game.edit.is-active')
            ->setDefaultValue(true);

        $form->addImageWithRFM('image_preview', 'form.camp-wide-game.edit.image-preview');

        // BUTTON
        $form->addSubmit('send', 'form.camp-wide-game.edit.send');
        $form->addSubmit('send_back', 'form.camp-wide-game.edit.send-back');
        $form->addSubmit('back', 'form.camp-wide-game.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->image_preview, BaseControl::DIR_IMAGE);

        if ($this->campWideGame->isLoaded()) {
            if ($identifier !== null && $this->campWideGame->getImagePreview() !== null) {
                $this->imageStorage->delete($this->campWideGame->getImagePreview());
            }

            $campWideGame = $this->facade->update(
                $this->campWideGame->getId(),
                $values->name,
                $values->content,
                $values->is_active,
                $identifier
            );
            $this->onFlashmessage('form.camp-wide-game.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $campWideGame = $this->facade->create(
                $values->name,
                $values->content,
                $values->is_active,
                $identifier
            );
            $this->onFlashmessage('form.camp-wide-game.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit',
            'id'      => $campWideGame->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ]);
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->campWideGame->isLoaded()) {
            return [];
        }

        return [
            'name'      => $this->campWideGame->getName(),
            'content'   => $this->campWideGame->getContent(),
            'is_active' => $this->campWideGame->isActive(),
        ];
    }
}
