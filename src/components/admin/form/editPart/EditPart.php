<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Components\Admin;

use App\Components\Form\FormWithUserControl;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\CampWideGame\BaseControl;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGame;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGameFacade;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGamePart;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGamePartFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

/**
 * Class EditPart
 */
class EditPart extends FormWithUserControl
{
    use APackageControl;

    /** @var LoaderFactory */
    public $webLoader;

    /** @var CampWideGamePartFacade */
    private $facade;

    /** @var CampWideGameFacade */
    private $facadeCampWideGame;

    /** @var CampWideGamePart */
    private $campWideGamePart;

    public function __construct(?int $id, CampWideGamePartFacade $facade, CampWideGameFacade $facadeCampWideGame, Translator $translator, LoggedUser $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);
        $this->facade             = $facade;
        $this->facadeCampWideGame = $facadeCampWideGame;
        $this->webLoader          = $webLoader;

        $this->campWideGamePart = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    /**
     * @return SimpleTranslation|string
     */
    public function getTitle()
    {
        if ($this->campWideGamePart->isLoaded()) {
            return new SimpleTranslation('form.camp-wide-game-part.edit.title - %s', $this->campWideGamePart->getName());
        }

        return 'form.camp-wide-game-part.edit.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [$this->webLoader->createJavaScriptLoader('adminTinyMce')];
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        $campWideGames        = [];
        $campWideGamesExclude = [];

        foreach ($values->camp_wide_games as $campWideGameId) {
            $campWideGames[] = $this->facadeCampWideGame->get(intval($campWideGameId));
        }

        foreach ($values->camp_wide_games_exclude as $campWideGameId) {
            $campWideGamesExclude[] = $this->facadeCampWideGame->get(intval($campWideGameId));
        }

        if ($this->campWideGamePart->isLoaded()) {
            $campWideGamePart = $this->facade->update(
                $this->campWideGamePart->getId(),
                $values->name,
                $values->content,
                $values->is_active,
                $values->is_for_all_camp_wide_game,
                $campWideGames,
                $campWideGamesExclude
            );
            $this->onFlashmessage('form.camp-wide-game-part.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $campWideGamePart = $this->facade->create(
                $values->name,
                $values->content,
                $values->is_active,
                $values->is_for_all_camp_wide_game,
                $campWideGames,
                $campWideGamesExclude
            );
            $this->onFlashmessage('form.camp-wide-game-part.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'send_back') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'edit-part',
            'id'      => $campWideGamePart->getId(),
        ]);
    }

    public function processOnBack() : void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render'  => 'overview-part',
        ]);
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/editPart.latte');

        $template->campWideGamePart = $this->campWideGamePart;
        $template->render();
    }

    protected function createComponentForm() : Form
    {
        // DATA
        $dataCampWideGames = $this->facadeCampWideGame->getPairs('id', 'name');

        // FORM
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.camp-wide-game-part.edit.name')
            ->setRequired('form.camp-wide-game-part.edit.name.req');
        $form->addTextArea('content', 'form.camp-wide-game-part.edit.content');

        $form->addCheckbox('is_active', 'form.camp-wide-game-part.edit.is-active')
            ->setDefaultValue(true);

        // CAMP-WIDE GAME
        $form->addMultiSelect('camp_wide_games', 'form.camp-wide-game-part.edit.camp-wide-games', $dataCampWideGames);
        $form->addMultiSelect('camp_wide_games_exclude', 'form.camp-wide-game-part.edit.camp-wide-games-exclude', $dataCampWideGames);
        $form->addCheckbox('is_for_all_camp_wide_game', 'form.camp-wide-game-part.edit.is-for-all-camp-wide-game')
            ->setDefaultValue(true)
            ->addCondition(Form::EQUAL, false)
            ->toggle('toggle-camp-wide-games')
            ->endCondition()
            ->addCondition(Form::EQUAL, true)
            ->toggle('toggle-camp-wide-games-exclude');

        // BUTTON
        $form->addSubmit('send', 'form.camp-wide-game-part.edit.send');
        $form->addSubmit('send_back', 'form.camp-wide-game-part.edit.send-back');
        $form->addSubmit('back', 'form.camp-wide-game-part.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults() : array
    {
        if (! $this->campWideGamePart->isLoaded()) {
            return [];
        }

        $campWideGames = $this->campWideGamePart->getCampWideGames()->map(function (CampWideGame $cwg) : int {
            return $cwg->getId();
        })->toArray();

        $campWideGamesExclude = $this->campWideGamePart->getCampWideGamesExclude()->map(function (CampWideGame $cwg) : int {
            return $cwg->getId();
        })->toArray();

        return [
            'name'                      => $this->campWideGamePart->getName(),
            'content'                   => $this->campWideGamePart->getContent(),
            'is_active'                 => $this->campWideGamePart->isActive(),
            'is_for_all_camp_wide_game' => $this->campWideGamePart->isForAllCampWideGame(),
            'camp_wide_games'           => $campWideGames,
            'camp_wide_games_exclude'   => $campWideGamesExclude,
        ];
    }
}
