<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Components\Admin;

/**
 * Interface IEditPartFactory
 */
interface IEditPartFactory
{
    public function create(?int $id = null) : EditPart;
}
