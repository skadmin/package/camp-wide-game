<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\CampWideGame\BaseControl;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGamePart;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGamePartFacade;
use Skadmin\Translator\Translator;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;

class OverviewPart extends GridControl
{
    use APackageControl;
    use IsActive;

    /** @var CampWideGamePartFacade */
    private $facade;

    /** @var LoaderFactory */
    private $webLoader;

    public function __construct(CampWideGamePartFacade $facade, Translator $translator, User $user, LoaderFactory $webLoader)
    {
        parent::__construct($translator, $user);

        $this->facade    = $facade;
        $this->webLoader = $webLoader;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overviewPart.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'camp-wide-game.overview-part.title';
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs() : array
    {
        return [
            $this->webLoader->createJavaScriptLoader('jQueryUi'),
        ];
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->orderBy('a.sequence', 'ASC'));

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator) : string {
            return $translator->translate($text);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.camp-wide-game.overview-part.name')
            ->setRenderer(function (CampWideGamePart $campWideGamePart) : Html {
                if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render'  => 'edit-part',
                        'id'      => $campWideGamePart->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href'  => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($campWideGamePart->getName());

                return $name;
            });
        $this->addColumnIsActive($grid, 'camp-wide-game.overview-part');

        // FILTER
        $grid->addFilterText('name', 'grid.camp-wide-game.overview-part.name');
        $this->addFilterIsActive($grid, 'camp-wide-game.overview-part');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.camp-wide-game.overview-part.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit-part',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#2', 'grid.camp-wide-game.overview-part.action.overview', [
            'package' => new BaseControl(),
            'render'  => 'overview',
        ])->setIcon('list-ul')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.camp-wide-game.overview-part.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit-part',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // SORTING
        $grid->setSortable();
        $grid->setSortableHandler($this->link('sort!'));

        return $grid;
    }

    public function handleSort(?string $item_id, ?string $prev_id, ?string $next_id) : void
    {
        $this->facade->sort($item_id, $prev_id, $next_id);

        $presenter = $this->getPresenterIfExists();
        if ($presenter !== null) {
            $presenter->flashMessage('grid.camp-wide-game.overview-part.action.flash.sort.success', Flash::SUCCESS);
        }

        $this['grid']->reload();
    }
}
