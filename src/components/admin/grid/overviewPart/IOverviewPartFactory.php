<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Components\Admin;

interface IOverviewPartFactory
{
    public function create() : OverviewPart;
}
