<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\ABaseControl;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Flash;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette\Application\UI\Presenter;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGamePartFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\CampWideGame\BaseControl;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGame;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGameFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\Utils\Utils\Colors\Colors;
use Ublaboo\DataGrid\Column\Action\Confirmation\CallbackConfirmation;
use function sprintf;

/**
 * Class Overview
 */
class Overview extends GridControl
{
    use APackageControl;

    /** @var CampWideGameFacade */
    private $facade;

    /** @var CampWideGamePartFacade */
    private $facadeCampWideGamePart;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(CampWideGameFacade $facade, CampWideGamePartFacade $facadeCampWideGamePart, Translator $translator, User $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);

        $this->facade                 = $facade;
        $this->facadeCampWideGamePart = $facadeCampWideGamePart;
        $this->imageStorage           = $imageStorage;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'camp-wide-game.overview.title';
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()->orderBy('a.createdAt', 'DESC'));

        // DATA
        $translator = $this->translator;
        $dialYesNo  = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator) : string {
            return $translator->translate($text);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.camp-wide-game.overview.name')
            ->setRenderer(function (CampWideGame $campWideGame) : Html {
                if ($campWideGame->getImagePreview() !== null) {
                    $img   = $this->imageStorage->fromIdentifier([$campWideGame->getImagePreview(), '36x36', 'exact']);
                    $elImg = Html::el('img', ['src' => '/' . $img->createLink()]);
                }

                $link = $this->getPresenter()->link('Component:default', [
                    'package' => new BaseControl(),
                    'render'  => 'edit',
                    'id'      => $campWideGame->getId(),
                ]);

                $href = Html::el('a', [
                    'href'  => $link,
                    'class' => 'font-weight-bold',
                ]);

                if (isset($elImg)) {
                    $href->addHtml($elImg)
                        ->addText(' ');
                }

                $href->addText($campWideGame->getName());

                return $href;
            });
        $grid->addColumnText('parts', 'grid.camp-wide-game.overview.parts')
            ->setRenderer(function (CampWideGame $campWideGame) : string {
                $parts = [];

                foreach ($this->facadeCampWideGamePart->findForCampWideGame($campWideGame) as $cwgp){
                    $parts[] = $cwgp->getName();
                }

                return implode(', ', $parts);
            });
        $grid->addColumnText('isActive', 'grid.camp-wide-game.overview.is-active')
            ->setAlign('center')
            ->setReplacement($dialYesNo);

        // FILTER
        $grid->addFilterText('name', 'grid.camp-wide-game.overview.name');
        $grid->addFilterSelect('isActive', 'grid.camp-wide-game.overview.is-active', Constant::PROMTP_ARR + Constant::DIAL_YES_NO)
            ->setTranslateOptions();

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addAction('edit', 'grid.camp-wide-game.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::DELETE)) {
            $confirmation   = new CallbackConfirmation(function (CampWideGame $campWideGame) {
                $translation = new SimpleTranslation('grid.camp-wide-game.overview.action.remove.confirmation - %s', $campWideGame->getName());
                return $this->translator->translate($translation);
            });
            $grid->addActionCallback('remove', 'grid.camp-wide-game.overview.action.remove')
                ->setConfirmation($confirmation)
                ->setIcon('trash-alt')
                ->setTitle('grid.camp-wide-game.overview.action.remove.title')
                ->setClass('btn btn-xs btn-outline-danger ajax')
                ->onClick[] = function ($id) {
                $campWideGame = $this->facade->get(intval($id));

                $campWideGameName = $campWideGame->getName();
                if ($this->facade->remove($campWideGame)) {
                    $flashMessage = new SimpleTranslation('grid.camp-wide-game.overview.action.flash.remove.success "%s"', $campWideGameName);
                    $type         = Flash::SUCCESS;
                } else {
                    $flashMessage = new SimpleTranslation('grid.camp-wide-game.overview.action.flash.remove.danger "%s"', $campWideGameName);
                    $type         = Flash::DANGER;
                }

                if ($this->getPresenterIfExists() !== null) {
                    $this->getPresenter()->flashMessage($flashMessage, $type);
                }

                $this['grid']->reload();
            };
        }

        // TOOLBAR
        $grid->addToolbarButton('Component:default#2', 'grid.camp-wide-game.overview.action.overview-part', [
            'package' => new BaseControl(),
            'render'  => 'overview-part',
        ])->setIcon('stream')
            ->setClass('btn btn-xs btn-outline-primary');

        if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $grid->addToolbarButton('Component:default', 'grid.camp-wide-game.overview.action.new', [
                'package' => new BaseControl(),
                'render'  => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        $grid->addToolbarSeo(BaseControl::RESOURCE, $this->getTitle());

        return $grid;
    }
}
