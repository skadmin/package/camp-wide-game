<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Components\Front;

/**
 * Interface IOverviewFactory
 */
interface IOverviewFactory
{
    public function create() : Overview;
}
