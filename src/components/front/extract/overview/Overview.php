<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use SkadminUtils\ImageStorage\ImageStorage;
use Nette;
use Nette\Application\UI\Presenter;
use Nette\ComponentModel\IContainer;
use Skadmin\CampWideGame\BaseControl;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGameFacade;
use Skadmin\Translator\Translator;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGameFilter;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGamePart;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGamePartFacade;
use SkadminUtils\Core\Components\Helpful\IPaginatorFactory;
use SkadminUtils\Core\Components\Helpful\TPaginatorOverview;
use SkadminUtils\FormControls\UI\Form;

class Overview extends TemplateControl
{
    use APackageControl;
    use TPaginatorOverview;

    private const COUNT = 12;

    /** @var CampWideGameFacade */
    private $facade;

    /** @var CampWideGamePartFacade */
    private $facadePart;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(CampWideGameFacade $facade, CampWideGamePartFacade $facadePart, Translator $translator, ImageStorage $imageStorage, IPaginatorFactory $iPaginatorFactory)
    {
        parent::__construct($translator);
        $this->facade     = $facade;
        $this->facadePart = $facadePart;

        $this->imageStorage = $imageStorage;
        $this->paginator    = $iPaginatorFactory->create();
    }

    public function setParent(?IContainer $parent, string $name = null)
    {
        parent::setParent($parent, $name);

        $this->page = intval($this->getParameter('page', 1));

        if ($this->getPresenterIfExists() instanceof Presenter) {
            $this->paginator->setPaginator(
                $this->facade->getCount(),
                self::COUNT,
                $this->page,
            );
        }

        return $this;
    }

    public function getTitle() : string
    {
        return 'camp-wide-game.front.overview.title';
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');

        $campWideGames = $this->facade->findCampWideGame(self::COUNT, $this->paginator->getOffset());

        $campWideGameParts = [];
        foreach ($campWideGames as $campWideGame) {
            $campWideGameParts[$campWideGame->getId()] = $this->facadePart->findForCampWideGame($campWideGame);
        }

        $template->campWideGames     = $campWideGames;
        $template->campWideGameParts = $campWideGameParts;
        $template->imageStorage      = $this->imageStorage;
        $template->package           = new BaseControl();

        $template->render();
    }

    public function createLink() : string
    {
        return $this->link('this', [
            'page' => $this->page === 1 ? null : $this->page,
        ]);
    }

}
