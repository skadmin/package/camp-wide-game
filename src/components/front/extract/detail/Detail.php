<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use SkadminUtils\ImageStorage\ImageStorage;
use Skadmin\CampWideGame\BaseControl;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGame;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGameFacade;
use Skadmin\CampWideGame\Doctrine\CampWideGame\CampWideGamePartFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;

/**
 * Class Detail
 */
class Detail extends TemplateControl
{
    use APackageControl;

    /** @var CampWideGameFacade */
    private $facade;

    /** @var CampWideGamePartFacade */
    private $facadeCampWideGamePart;

    /** @var CampWideGame */
    private $campWideGame;

    /** @var ImageStorage */
    private $imageStorage;

    public function __construct(int $id, CampWideGameFacade $facade, CampWideGamePartFacade $facadeCampWideGamePart, Translator $translator, ImageStorage $imageStorage)
    {
        parent::__construct($translator);
        $this->facade                 = $facade;
        $this->facadeCampWideGamePart = $facadeCampWideGamePart;

        $this->campWideGame = $this->facade->get($id);

        $this->imageStorage = $imageStorage;
    }

    public function getTitle() : SimpleTranslation
    {
        return new SimpleTranslation('camp-wide-game.front.detail.title - %s', $this->campWideGame->getName());
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/detail.latte');

        $template->campWideGame      = $this->campWideGame;
        $template->campWideGameParts = $this->facadeCampWideGamePart->findForCampWideGame($this->campWideGame);
        $template->imageStorage      = $this->imageStorage;
        $template->package           = new BaseControl();

        $template->render();
    }
}
