<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Doctrine\CampWideGame;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Nette\Utils\Strings;
use Skadmin\CampWideGame\Model\Helper;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class CampWideGame
{
    use Entity\BaseEntity;
    use Entity\IsActive;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\ImagePreview;

    public function update(string $name, string $content, bool $isActive, ?string $imagePreview): void
    {
        $this->name    = $name;
        $this->content = $content;

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }

        $this->setIsActive($isActive);
    }

}
