<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Doctrine\CampWideGame;

use SkadminUtils\DoctrineTraits\Facade;
use DateTimeInterface;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Nette\Utils\DateTime;
use Nettrine\ORM\EntityManagerDecorator;
use function intval;

/**
 * Class CampWideGameFacade
 */
final class CampWideGameFacade extends Facade
{
    use Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = CampWideGame::class;
    }

    public function create(string $name, string $content, bool $isActive, ?string $imagePreview) : CampWideGame
    {
        return $this->update(null, $name, $content, $isActive, $imagePreview);
    }

    public function update(?int $id, string $name, string $content, bool $isActive, ?string $imagePreview) : CampWideGame
    {
        $campWideGame = $this->get($id);
        $campWideGame->update($name, $content, $isActive, $imagePreview);

        if (! $campWideGame->isLoaded()) {
            $campWideGame->setWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($campWideGame);
        $this->em->flush();

        return $campWideGame;
    }

    public function get(?int $id = null) : CampWideGame
    {
        if ($id === null) {
            return new CampWideGame();
        }

        $campWideGame = parent::get($id);

        if ($campWideGame === null) {
            return new CampWideGame();
        }

        return $campWideGame;
    }

    public function getCount() : int
    {
        $qb = $this->findCampWideGameQb();
        $qb->select('count(distinct a.id)');

        return intval($qb->getQuery()
            ->getSingleScalarResult());
    }

    /**
     * @return CampWideGame[]
     */
    public function findCampWideGame(?int $limit = null, ?int $offset = null) : array
    {
        return $this->findCampWideGameQb($limit, $offset)
            ->getQuery()
            ->getResult();
    }

    private function findCampWideGameQb(?int $limit = null, ?int $offset = null) : QueryBuilder
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a')
            ->distinct();

        $qb->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('a.createdAt', 'DESC');

        $criteria = Criteria::create();

        $qb->addCriteria($criteria);

        return $qb;
    }

    public function findByWebalize(string $webalize) : ?CampWideGame
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function remove(CampWideGame $campWideGame) : bool
    {
        if ($campWideGame->isLoaded()) {
            $this->em->remove($campWideGame);
            $this->em->flush();

            return true;
        }

        return false;
    }
}
