<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Doctrine\CampWideGame;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Types\Types;
use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class CampWideGamePart
{
    use Entity\Id;
    use Entity\IsActive;
    use Entity\WebalizeName;
    use Entity\Content;
    use Entity\Sequence;

    #[ORM\Column(type: Types::BOOLEAN, options: ['default' => false])]
    private bool $isForAllCampWideGame = false;

    /** @var ArrayCollection<int, CampWideGame> */
    #[ORM\ManyToMany(targetEntity: CampWideGame::class)]
    #[ORM\JoinTable(name: 'camp_wide_game_part_rel')]
    private $campWideGames;

    /** @var ArrayCollection<int, CampWideGame> */
    #[ORM\ManyToMany(targetEntity: CampWideGame::class)]
    #[ORM\JoinTable(name: 'camp_wide_game_part_exlude_rel')]
    private $campWideGamesExclude;

    public function __construct()
    {
        $this->campWideGames        = new ArrayCollection();
        $this->campWideGamesExclude = new ArrayCollection();
    }

    /**
     * @param array<CampWideGame> $campWideGames
     * @param array<CampWideGame> $campWideGamesExclude
     */
    public function update(string $name, string $content, bool $isActive, bool $isForAllCampWideGame, array $campWideGames, array $campWideGamesExclude): void
    {
        $this->name    = $name;
        $this->content = $content;

        // rels
        $this->isForAllCampWideGame = $isForAllCampWideGame;

        $this->campWideGames->clear();
        foreach ($campWideGames as $campWideGame) {
            if ($campWideGame instanceof CampWideGame && ! $this->campWideGames->contains($campWideGame)) {
                $this->campWideGames->add($campWideGame);
            }
        }

        $this->campWideGamesExclude->clear();
        foreach ($campWideGamesExclude as $campWideGame) {
            if ($campWideGame instanceof CampWideGame && ! $this->campWideGamesExclude->contains($campWideGame)) {
                $this->campWideGamesExclude->add($campWideGame);
            }
        }

        $this->setIsActive($isActive);
    }

    public function isForAllCampWideGame(): bool
    {
        return $this->isForAllCampWideGame;
    }

    /**
     * @return ArrayCollection|array<CampWideGame>
     */
    public function getCampWideGames()
    {
        return $this->campWideGames;
    }

    /**
     * @return ArrayCollection|array<CampWideGame>
     */
    public function getCampWideGamesExclude()
    {
        return $this->campWideGamesExclude;
    }

}
