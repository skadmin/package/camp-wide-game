<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame\Doctrine\CampWideGame;

use SkadminUtils\DoctrineTraits\Facade;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;

final class CampWideGamePartFacade extends Facade
{
    use Facade\Webalize;
    use Facade\Sequence;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = CampWideGamePart::class;
    }

    /**
     * @param array<CampWideGame> $campWideGames
     * @param array<CampWideGame> $campWideGamesExclude
     */
    public function create(string $name, string $content, bool $isActive, bool $isForAllCampWideGame, array $campWideGames, array $campWideGamesExclude) : CampWideGamePart
    {
        return $this->update(null, $name, $content, $isActive, $isForAllCampWideGame, $campWideGames, $campWideGamesExclude);
    }

    /**
     * @param array<CampWideGame> $campWideGames
     * @param array<CampWideGame> $campWideGamesExclude
     */
    public function update(?int $id, string $name, string $content, bool $isActive, bool $isForAllCampWideGame, array $campWideGames, array $campWideGamesExclude) : CampWideGamePart
    {
        $campWideGamePart = $this->get($id);
        $campWideGamePart->update($name, $content, $isActive, $isForAllCampWideGame, $campWideGames, $campWideGamesExclude);

        if (! $campWideGamePart->isLoaded()) {
            $campWideGamePart->setWebalize($this->getValidWebalize($name));
            $campWideGamePart->setSequence($this->getValidSequence());
        }

        $this->em->persist($campWideGamePart);
        $this->em->flush();

        return $campWideGamePart;
    }

    public function get(?int $id = null) : CampWideGamePart
    {
        if ($id === null) {
            return new CampWideGamePart();
        }

        $campWideGamePart = parent::get($id);

        if ($campWideGamePart === null) {
            return new CampWideGamePart();
        }

        return $campWideGamePart;
    }

    /**
     * @return CampWideGamePart[]
     */
    public function getAll(bool $onlyActive = false) : array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        $orderBy = ['sequence' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByWebalize(string $webalize) : ?CampWideGamePart
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    /**
     * @return array<CampWideGamePart>
     */
    public function findForCampWideGame(CampWideGame $campWideGame) : array
    {
        $campWideGameParts = [];

        // all
        /** @var QueryBuilder $qb */
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('a.isForAllCampWideGame', true));
        $criteria->andWhere(Criteria::expr()->eq('a.isActive', true));

        $_campWideGameParts = $qb->addCriteria($criteria)
            ->getQuery()->getResult();

        $campWideGameParts = array_merge_recursive($campWideGameParts, array_filter($_campWideGameParts, function (CampWideGamePart $cwgp) use ($campWideGame) : bool {
            return ! $cwgp->getCampWideGamesExclude()->contains($campWideGame);
        }));

        // inverse all

        /** @var QueryBuilder $qb */
        $qb = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a');

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('a.isForAllCampWideGame', false));
        $criteria->andWhere(Criteria::expr()->eq('a.isActive', true));

        $_campWideGameParts = $qb->addCriteria($criteria)
            ->getQuery()->getResult();

        $campWideGameParts = array_merge_recursive($campWideGameParts, array_filter($_campWideGameParts, function (CampWideGamePart $cwgp) use ($campWideGame) : bool {
            return $cwgp->getCampWideGames()->contains($campWideGame);
        }));

        return $campWideGameParts;
    }
}
