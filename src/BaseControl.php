<?php

declare(strict_types=1);

namespace Skadmin\CampWideGame;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE  = 'camp-wide-game';
    public const DIR_IMAGE = 'camp-wide-game';

    public function getMenu() : ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-campground']),
            'items'   => ['overview'],
        ]);
    }
}
